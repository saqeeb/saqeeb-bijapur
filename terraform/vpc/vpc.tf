provider "aws" {
  region= "us-east-1"
  access_key = "AKIAXRFVBZWQVSO7XLDI"
  secret_key = "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"
}

#create vpc
resource "aws_vpc" "my-tf-vpc" {
  cidr_block = var.vpc_cidr
}

#create public subnet1
resource "aws_subnet" "public_subnet1" {
  vpc_id = aws_vpc.my-tf-vpc.id
  cidr_block = var.public_subnet1
  map_public_ip_on_launch = true
  availability_zone = "us-east-1a"
  tags = {
      Name= "public-subnet1"
  }
  depends_on = [
    aws_vpc.my-tf-vpc
  ]
}

#create public subnet2
resource "aws_subnet" "public_subnet2" {
  vpc_id = aws_vpc.my-tf-vpc.id
  cidr_block = var.public_subnet2
  map_public_ip_on_launch = true
  availability_zone = "us-east-1b"
  tags = {
      Name= "public-subnet2"
  }
  depends_on = [
    aws_subnet.public_subnet1
  ]
}

#create private subnet1
resource "aws_subnet" "private_subnet1" {
  vpc_id = aws_vpc.my-tf-vpc.id
  cidr_block = var.private_subnet1
  availability_zone = "us-east-1a"
  tags = {
      Name= "private-subnet1"
  }
  depends_on = [
    aws_subnet.public_subnet2
  ]
}

#create private subnet2
resource "aws_subnet" "private_subnet2" {
  vpc_id = aws_vpc.my-tf-vpc.id
  cidr_block = var.private_subnet2
  availability_zone = "us-east-1b"
  tags = {
      Name= "private-subnet2"
  }
  depends_on = [
    aws_subnet.private_subnet1
  ]
}

#create internet gateway
resource "aws_internet_gateway" "my_int_gw" {
  vpc_id = aws_vpc.my-tf-vpc.id
  tags = {
    "Name" = "my-int-gw"
  }
}

#create public route table
resource "aws_route_table" "public_rt" {
    vpc_id = aws_vpc.my-tf-vpc.id
    route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.my_int_gw.id
  }

  route {
    ipv6_cidr_block        = "::/0"
    gateway_id = aws_internet_gateway.my_int_gw.id
  }

  tags = {
    Name = "public_rt"
  }
  
}

# public route-table subnet1 association
resource "aws_route_table_association" "public-sbnt1" {
    subnet_id = aws_subnet.public_subnet1.id
    route_table_id = aws_route_table.public_rt.id
}

# public route-table subnet2 association
resource "aws_route_table_association" "public-sbnt2" {
    subnet_id = aws_subnet.public_subnet2.id
    route_table_id = aws_route_table.public_rt.id
}

#create nat ip
resource "aws_eip" "nat_ip" {
  vpc = true
}

#create nat gateway
resource "aws_nat_gateway" "nat_gateway" {
  allocation_id = aws_eip.nat_ip.id
  subnet_id = aws_subnet.public_subnet1.id
  depends_on = [
    aws_eip.nat_ip
  ]
}

#create private route table
resource "aws_route_table" "private_rt" {
    vpc_id = aws_vpc.my-tf-vpc.id
    route {
    cidr_block = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.nat_gateway.id
  }
  tags = {
    Name = "private_rt"
  }
}

#private subnet1 to private-rt association
resource "aws_route_table_association" "private-sbnt1" {
    subnet_id = aws_subnet.private_subnet1.id
    route_table_id = aws_route_table.private_rt.id
}

#private subnet2 to private-rt association
resource "aws_route_table_association" "private-sbnt2" {
    subnet_id = aws_subnet.private_subnet2.id
    route_table_id = aws_route_table.private_rt.id
}













































